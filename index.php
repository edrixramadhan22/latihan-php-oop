<?php
    require_once ('hewan.php');
    require_once ('kodok.php');
    require_once ('kera.php');

    $kambing = new hewan ("shaun");
    echo "Nama : " .$kambing->nama. "<br>";
    echo "Kaki : " .$kambing->kaki. "<br>"; 
    echo "Cold Blooded : " .$kambing->cold_blooded. "<br><br>";

    $kodok = new kodok ("buduk");
    echo "Nama : " .$kodok->nama. "<br>";
    echo "Kaki : " .$kodok->kaki. "<br>";
    echo "Cold Blooded : " .$kodok->cold_blooded. "<br>";
    echo "Jump : " .$kodok->jump. "<br><br>";

    $kera = new kera ("kera sakti");
    echo "Nama : " .$kera->nama. "<br>";
    echo "Kaki : " .$kera->kaki. "<br>";
    echo "Cold Blooded : " .$kera->cold_blooded. "<br>";
    echo "Yell : " .$kera->yell. "<br>";
?>